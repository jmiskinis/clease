import json
import ase
from percopop import Input, Lattice, Percolator
import numpy as np
from clease.datastructures import MCStep
from clease.montecarlo import observers


class PercolationAnalysisObserver(observers.mc_observer.MCObserver):
    """
    Analyzes percolation properties of structures.

    Args:
        atoms: Atoms object
            Structure used in Monte Carlo.

        settings: dict
            Settings containing parameters for percolation analysis.
    """

    name = "PercolationAnalysis"

    def __init__(self, atoms: ase.Atoms, settings: dict):
        super().__init__()

        self.paths = {}
        self.num_samples = 0
        self.aps = []
        self.interval = settings["percolation"]["interval"]
        self.input = Input.from_string(self.get_parameters(), atoms=atoms)
        lattice = Lattice.from_input_object(self.input, supercell=[1, 1, 1])
        self.percolator = Percolator.from_input_object(self.input, lattice)
        active_coords = np.dot(np.array(self.percolator._coo), self.percolator._avec)
        static_coords = self.input.static_sites.cart_coords.copy()  # type: ignore

        self.structure = {
            "cell": np.transpose(self.percolator._avec).flatten(),
            "positions": np.vstack((active_coords, static_coords)),
        }

    def reset(self):
        """
        Resets the counters.
        """

        self.num_samples = 0
        self.aps = []

    def _analyze_percolation(self) -> list:
        """
        Performs percolation analysis of a structure.

        Args:
            mc_step: MCStep
                Container with information about a single MC step.

        Returns:
            `True` if there is at least one percolating network.
        """

        self.percolator.reset(
            self.percolator.lattice.occupied,
            [self.percolator.lattice.species[i] for i in self.percolator.lattice.occupied]
        )

        static_species = [str(specie) for specie in self.input.static_sites.species]

        self.paths = {
            "symbols": self.percolator.lattice.species + static_species,
            "clusters": []
        }

        for cluster in self.percolator.percolating_clusters:
            network = self.percolator.get_sites_of_cluster(cluster)
            paths = self.percolator.generate_percolating_path(network)
            wrapping = self.percolator.wrapping[cluster]

            self.paths["clusters"].append({
                "size": len(network),
                "wrapping": wrapping,
                "paths": paths,
            })

        return self.percolator.accessible_percolating_species()

    def observe_step(self, mc_step: MCStep) -> None:
        """
        Called after every Monte Carlo step.

        Args:
            mc_step: MCStep
                Container with information about a single MC step.
        """

        if mc_step.step == 1:
            self.reset()

        if mc_step.move_accepted:
            for move in mc_step.last_move:
                if move.old_symb in ["O", "F"]:
                    # Find corresponding site in reduced structure:
                    mapped_index = self.input.static_sites_dict.get(move.index, None)

                    if mapped_index is None:
                        raise KeyError(
                            "Could not find corresponding atom in reduced structure."
                        )

                    self.input.static_sites.replace(mapped_index, move.new_symb)
                elif move.old_symb in ["Li", "Mn", "Ti"]:
                    # Find corresponding site in reduced structure:
                    mapped_index = self.input.active_sites_dict.get(move.index, None)

                    if mapped_index is None:
                        raise KeyError(
                            "Could not find corresponding atom in reduced structure."
                        )

                    self.percolator.lattice.set_site(mapped_index, move.new_symb)

        if mc_step.step % self.interval == 0:
            self.num_samples += 1
            aps = self._analyze_percolation()
            self.aps.append(aps)

    def get_averages(self) -> dict[str, float]:
        """
        Returns dictionary with percolation analysis results.
        """

        return {
            "0_tm_aps": format(sum(self.aps) / self.num_samples, ".3f")
        }

    def get_parameters(self):
        """
        Sets parameters for percolation analysis.

        Args:
            settings: dict
                Settings containing parameters for percolation analysis.

        Returns:
            A JSON string containing parameters for percolation analysis.
        """

        return json.dumps({
            "formula_units": 1,
            "sublattices": {
                "oct": {
                    "description": "octahedral site",
                    "sites": {
                        "species": [
                            "Li",
                            "Mn",
                            "Ti"
                        ]
                    }
                },
                "oxyf": {
                    "description": "oxygen and fluoride sites",
                    "sites": {
                        "species": [
                            "O",
                            "F"
                        ]
                    },
                    "ignore": True
                }
            },
            "percolating_species": ["Li"],
            "bonds": [
                {
                    "sublattices": [
                        "oct",
                        "oct"
                    ],
                    "bond_rules": [
                        [
                            "ZeroTMNeighborsBR"
                        ]
                    ]
                }
            ],
        })
